using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

    // Complete the closestNumbers function below.
    static int[] closestNumbers(int[] arr) {
        List<Tuple<int,int>> list_number = new List<Tuple<int,int>>();
        List<int> final_list = new List<int>();
        int diff = 100000;
        for ( int i = 0 ; i < arr.Count() ; i++ )
            for ( int j = 0 ; j < arr.Count() ; j++ )
                if ( i != j && Math.Abs(arr[i]-arr[j]) < diff){
                    if (!list_number.Contains(new Tuple<int,int>(arr[i],arr[j])))
                        list_number.Add(new Tuple<int,int>(arr[i],arr[j]));
                                             
                    diff = Math.Abs(arr[i]-arr[j]);                  
                }
     
        if (list_number.Count() > 1){
            
            for ( int i = 0 ; i < list_number.Count() ; i++ )
               if ( diff == Math.Abs(list_number[i].Item2-list_number[i].Item1)){
                    
                    if ( !final_list.Contains(list_number[i].Item1))
                        final_list.Add(list_number[i].Item1);

                    if ( !final_list.Contains(list_number[i].Item2))
                        final_list.Add(list_number[i].Item2);
                   
               }
        
            final_list.Sort();  
            return final_list.ToArray();
        }
        else{
            int[] a = new int[]{list_number[0].Item1,list_number[0].Item2};
            return a ;
        }
    

    }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int n = Convert.ToInt32(Console.ReadLine());

        int[] arr = Array.ConvertAll(Console.ReadLine().Split(' '), arrTemp => Convert.ToInt32(arrTemp))
        ;
        int[] result = closestNumbers(arr);

        textWriter.WriteLine(string.Join(" ", result));

        textWriter.Flush();
        textWriter.Close();
    }
}
