using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

  
    // Complete the gameOfThrones function below.
    static string gameOfThrones(string s) { 
        Dictionary<char, int> d = new Dictionary<char, int>();
        for (int i = 0 ; i < s.Length ; i++ )
            if ( !d.ContainsKey(s[i]) ){
                d.Add(s[i],1);
            }
            else{
                d[s[i]] += 1;
            }
   
        if ( s.Length % 2 != 0 ){ // str is odd
            int count_odd = 0;
            foreach ( KeyValuePair<char,int> c in d )
                if ( c.Value % 2 != 0)
                    count_odd ++;
            
            if ( count_odd == 1)
                return "YES";
        
            else    
                return "NO";
        }  
        
        if ( s.Length % 2 == 0 ){ // str is odd
            int count_even = 0;
            foreach ( KeyValuePair<char,int> c in d )
                if ( c.Value % 2 == 0)
                    count_even++;
            
            if ( count_even == d.Count() )
                return "YES";
        
            else    
                return "NO";
        }  
    
        return null;
    }
    
    
    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = gameOfThrones(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
